<?php
namespace Core;
defined("APPPATH") OR die("Access denied");

use \Core\App;

/**
 * @class Database
 */
class Database {

	/**
	* @desc nombre del usuario de la base de datos
	* @var $_user
	* @access private
	*/
	private $_user;

	/**
	* @desc password de la base de datos
	* @var $_password
	* @access private
	*/
	private $_password;

	/**
	* @desc nombre del host
	* @var $_host
	* @access private
	*/
	private $_host;

	/**
	* @desc nombre de la base de datos
	* @var $_db
	* @access protected
	*/
	protected $_db;

	/**
	* @desc conexión a la base de datos
	* @var $_connection
	* @access private
	*/
	private $_connection;

    /**
    * @desc instancia de la base de datos
    * @var $_instance
    * @access private
    */
    private static $_instance;

	/**
	 * [__construct]
	 */
    private function __construct() {
       try {
		   //load from config/config.ini
		   $config = App::getConfig();
		   $this->_host = $config["host"];
		   $this->_user = $config["user"];
		   $this->_password = $config["password"];
		   $this->_db = $config["database"];

           $this->_connection = new \PDO('mysql:host='.$this->_host.'; dbname='.$this->_db, $this->_user, $this->_password);
           $this->_connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
           $this->_connection->exec("SET CHARACTER SET utf8");
       }
       catch (\PDOException $e) {
           print "Error!: " . $e->getMessage();
           die();
       }
    }

	/**
	 * [prepare]
	 * @param  [type] $sql [description]
	 * @return [type]      [description]
	 */
	public function prepare($sql) {
        return $this->_connection->prepare($sql);
    }

	/**
	 * [instance singleton]
	 * @return [object] [class database]
	 */
    public static function instance() {
        if (!isset(self::$_instance)) {
            $class = __CLASS__;
            self::$_instance = new $class;
        }
        return self::$_instance;
    }

    /**
     * [__clone Evita que el objeto se pueda clonar]
     * @return [type] [message]
     */
    public function __clone() {
        trigger_error('La clonación de este objeto no está permitida', E_USER_ERROR);
    }
}
?>