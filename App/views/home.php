<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title><?php echo $title; ?></title>
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<th>ID</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($users as $user) { ?>
				<tr>
					<td><?php echo $user['id']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</body>
</html>