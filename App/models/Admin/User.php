<?php
namespace App\Models\Admin;
defined("APPPATH") OR die("Access denied");

use \Core\Database;
use \App\Interfaces\Crud;

class User implements Crud {

    public static function create($data) {

    }

    public static function read($id) {
        try {
            $connection = Database::instance();
            $sql = "SELECT * FROM `users` WHERE `id` = ?";
            $query = $connection->prepare($sql);
            $query->bindParam(1, $id, \PDO::PARAM_INT);
            $query->execute();
            return $query->fetch();
        }
        catch(\PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
    }

    public static function update($data) {

    }

    public static function delete($id) {

    }

    public static function getAll() {
        try {
			$connection = Database::instance();
			$sql = "SELECT * FROM users";
			$query = $connection->prepare($sql);
			$query->execute();
			return $query->fetchAll();
		}
        catch(\PDOException $e)
        {
			print "Error!: " . $e->getMessage();
		}
    }
}
?>