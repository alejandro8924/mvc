<?php
namespace App\Controllers;
defined("APPPATH") OR die("Access denied");

use \Core\View,
    \App\Models\User,
    \App\Models\Admin\User as UserAdmin;

class Home {
    public function saludo($nombre) {
        View::set("name", $nombre);
        View::set("title", "Custom MVC");
        View::render("home");
    }

    public function users() {
        $users = UserAdmin::getAll();
        View::set("users", $users);
        View::set("title", "Custom MVC");
        View::render("home");
    }
}
?>